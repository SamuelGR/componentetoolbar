{
  const {
    html,
  } = Polymer;
  /**
    `<cells-lv-toolbar>` Description.

    Example:

    ```html
    <cells-lv-toolbar></cells-lv-toolbar>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-lv-toolbar | :host    | {} |

    * @customElement cells-lv-toolbar
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsLvToolbar extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-lv-toolbar';
    }
    static get properties() {
      return {
        options: {
          type: Array,
          value: ()=>([])
        }
      };
    }
    _isEqualTo(option, value) {
      return option === value;
    }

  }

  customElements.define(CellsLvToolbar.is, CellsLvToolbar);
}